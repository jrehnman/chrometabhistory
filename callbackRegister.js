
"use strict"

var g_navigation = {}


function onMessage(message, sender, sendResponse)
{
    if (sender.tab && (sender.tab.id in g_navigation))
    {
        let storedhistory = g_navigation[sender.tab.id]
        sendResponse(storedhistory)
    }
}

chrome.runtime.onMessage.addListener(onMessage)

function onNewTab(tab)
{
    if (!(tab.id in g_navigation))
    {
        g_navigation[tab.id] = []
    }

    if (tab.openerTabId && tab.openerTabId in g_navigation)
    {
        let parenthistory = g_navigation[tab.openerTabId]
        for (let navigation of parenthistory)
        {
            g_navigation[tab.id].push(navigation)
        }
    }
}

chrome.tabs.onCreated.addListener(onNewTab)

function onTabClosed(tabid, reason)
{
    delete g_navigation[tabid]
}

chrome.tabs.onRemoved.addListener(onTabClosed)

function intersection(as, bs)
{
    return as.filter(function(a){ return (bs.indexOf(a) >= 0) })
}

function logNavigation(details)
{
    var recorded_transitions = ["link", "typed", "auto_bookmark", "generated", "start_page", "form_submit", "keyword", "keyword_generated"]
    var ignored_qualifiers = ["forward_back"]

    var is_recorded = recorded_transitions.indexOf(details.transitionType) >= 0
    var is_ignored = intersection(ignored_qualifiers, details.transitionQualifiers).length > 0

    
    if (is_recorded && !is_ignored) 
    {
        if (!(details.tabId in g_navigation))
        {
            g_navigation[details.tabId] = []
        }

        g_navigation[details.tabId].push({'url': details.url, 'title': null})
    }
}

function logTitle(details)
{
    function onTab(tab)
    {
        let navigation = g_navigation[tab.id].pop()
        if (navigation)
        {
            if ((tab.url == navigation.url) && !navigation.title)
            {
                navigation.title = tab.title
            }
            g_navigation[tab.id].push(navigation)
        }
    }

    if (details.tabId in g_navigation)
    {
        chrome.tabs.get(details.tabId, onTab)
    }
}

chrome.webNavigation.onCommitted.addListener(logNavigation)
chrome.webNavigation.onDOMContentLoaded.addListener(logTitle)

