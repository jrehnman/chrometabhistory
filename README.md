# README #
Chrome extension retaining history between tabs.

### Known issues ###
* All history from previous tab will be copied to new tab, not just the items in "back" buffer.

