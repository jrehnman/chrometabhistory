
"use strict"


var statekey = 'tabhistory_' + chrome.runtime.id

function reconstructHistory(history)
{
    var current_title = document.title
    var current_url = window.location.href

    var first = true
    for (let navigation of history)
    {
        let stateobj = {}
        stateobj[statekey] = navigation.url

        if (first)
        {
            window.history.replaceState(stateobj, navigation.title, '')
            first = false
        }
        else
        {
            window.history.pushState(stateobj, navigation.title, '')
        }
        document.title = navigation.title
    }
}

function restoryHistoryItem(state)
{
    let url = state[statekey]
    window.location.replace(url)
    window.history.replaceState(state, '', url) // location.replace for some reason erases all earlier state:s in the history unless history.replace is also used???
}

window.onpopstate = function(e)
{
    if (e.state && statekey in e.state)
    {
        restoryHistoryItem(e.state)
    }
}

var is_new_tab = (window.history.length <= 1)
if (is_new_tab)
{
    chrome.runtime.sendMessage(null, {}, null, reconstructHistory)
}
else if (window.history.state && statekey in window.history.state && window.history.state[statekey] != window.location.href)
{
    restoryHistoryItem(window.history.state)
}

