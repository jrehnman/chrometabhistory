
.PHONY: clean

PACKAGE_NAME=tabhistory.zip
CONVERTED_ICONS=
LOCALES=$(wildcard _locales/*/messages.json)

%.png: %.xcf
	xcf2png $^ -o $@

$(PACKAGE_NAME): manifest.json callbackRegister.js $(CONVERTED_ICONS) $(LOCALES)
	zip -r $@ $^

clean:
	rm -vf $(PACKAGE_NAME) $(CONVERTED_ICONS)

